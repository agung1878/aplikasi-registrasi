/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aplikasi.registrasi.aplikasiregistrasi.dto;

import com.aplikasi.registrasi.aplikasiregistrasi.constant.RestResponseStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author agung
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestBaseResponse {
    private RestResponseStatus status;
    private String message;
    private Object data;

    public RestResponseStatus getStatus() {
        return status;
    }

    public void setStatus(RestResponseStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    
    
}
