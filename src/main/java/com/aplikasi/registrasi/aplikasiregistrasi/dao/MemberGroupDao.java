package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.constant.MemberGroupStatus;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.MemberGroup;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MemberGroupDao extends PagingAndSortingRepository<MemberGroup, String> {

    List<MemberGroup> findByUserMemberUser(User user);

    Page<MemberGroup> findByGroupIdAndMemberGroupStatus(String id, MemberGroupStatus status, Pageable page);

    MemberGroup findByGroupIdAndUserMemberId(String id, String UserMember);
}
