package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserAdmin;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserAdminDao extends PagingAndSortingRepository<UserAdmin, String> {
    public UserAdmin findByUserId(String id);
    public UserAdmin findByUserUsername(String username);
}
