package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.Group;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GroupDao extends PagingAndSortingRepository<Group, String> {

    List<Group> findByIdNotIn(Iterable<String> listId);

}
