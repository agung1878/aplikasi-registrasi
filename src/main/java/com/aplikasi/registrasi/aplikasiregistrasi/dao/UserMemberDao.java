package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author agung
 */
public interface UserMemberDao extends PagingAndSortingRepository<UserMember, String>{

    public UserMember findByUserId(String id);
    public UserMember findByUserUsername(String username);
    UserMember findByEmail(String email);
    UserMember findByPhone(String phone);
    
}
