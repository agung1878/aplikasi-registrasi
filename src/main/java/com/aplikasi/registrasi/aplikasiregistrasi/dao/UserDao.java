package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {
    User findByUsername(String username);

}
