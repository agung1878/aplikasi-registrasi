package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.Regional;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RegionalDao extends PagingAndSortingRepository<Regional, String> {

   
}
