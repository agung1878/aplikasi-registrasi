package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserPassword;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author agung
 */
public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String>{
    
}
