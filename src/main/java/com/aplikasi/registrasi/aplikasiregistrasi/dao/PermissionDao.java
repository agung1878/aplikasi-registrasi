package com.aplikasi.registrasi.aplikasiregistrasi.dao;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.Permission;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author dian
 */
public interface PermissionDao extends PagingAndSortingRepository<Permission, String> {
    
}
