package com.aplikasi.registrasi.aplikasiregistrasi.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageService {

    public static final String ORIGINALPREFIX = "ori-";

    public static final String PROFILE = "profile";

    public String uploadDir = "upload";

    public File moveFile(MultipartFile multipartFile, String type) {
        try {
            String folder = uploadDir + File.separator + type + File.separator;
            File folderImages = new File(folder);
            if (!folderImages.exists()) {
                Files.createDirectories(folderImages.toPath());
            }

            String random = UUID.randomUUID().toString().substring(0, 7);

            int len = multipartFile.getOriginalFilename().length();
            String fileName = multipartFile.getOriginalFilename();
            if (len > 20) {
                fileName = fileName.substring(len - 10);
            }

            int occurance = StringUtils.countOccurrencesOf(fileName, ".");

            if (occurance > 1) {
                for (int i = 0; i < occurance - 1; i++) {
                    fileName = fileName.replaceFirst("\\.", "-");
                }
            }

            fileName = fileName.replace(" ", "-");
            fileName = fileName.replace("_", "-");
            fileName = fileName.replaceAll("[^\\w\\-\\.]", "");
            String name = random + "-" + fileName;

            File originalFile = new File(folder + ORIGINALPREFIX + name);
            Files.copy(multipartFile.getInputStream(), originalFile.toPath());

            return originalFile;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private Path load(String folder, String filename) {
        return new File(uploadDir).toPath().resolve(folder + "/" + filename);
    }

    public Resource loadAsResource(String filename, String tpe) throws MalformedURLException {
        Path filePath = load(tpe, filename);
        Resource resource = new UrlResource(filePath.toUri());
        if (resource.exists() || resource.isReadable()) {
            return resource;
        } else {
            throw new MalformedURLException("Could not read file: " + filename);
        }
    }
}
