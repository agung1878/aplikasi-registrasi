package com.aplikasi.registrasi.aplikasiregistrasi.services;

import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    private JavaMailSender javaMailSender;

    @Autowired
    public NotificationService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendNotification(UserMember userMember) throws MailException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(userMember.getEmail());
        mail.setFrom("apliksiregister@gmail.com");
        mail.setSubject("Group Member");
        mail.setText("Permintaan anda untuk join group sudah di terima..");

        javaMailSender.send(mail);

    }

}
