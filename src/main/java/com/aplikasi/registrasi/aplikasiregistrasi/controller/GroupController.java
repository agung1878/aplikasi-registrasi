package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.CategoryDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.GroupDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.RegionalDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.Group;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/master/group")
public class GroupController {

    @Autowired
    private GroupDao groupDao;
    @Autowired
    private RegionalDao regionalDao;
    @Autowired
    private CategoryDao categoryDao;

    private static final String VIEW_LIST = "master/group/list";
    private static final String VIEW_FORM = "master/group/form";

    private final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault(size = 5) Pageable pageable) {
        mm.addAttribute("data", groupDao.findAll(pageable));

        return VIEW_LIST;
    }

    @GetMapping("/form")
    public String viewForm(ModelMap mm, @RequestParam(required = false) Optional<String> id) throws Exception {
        if (id.isPresent()) {
            try {
                groupDao.findById(id.get()).ifPresent(Group -> mm.addAttribute("group", Group));
            } catch (Exception e) {
                logger.error("Data tidak ditemukan");
                throw new Exception("Data tidak ditemukan");
            }
        } else {
            mm.addAttribute("group", new Group());
        }

        mm.addAttribute("listCategory", categoryDao.findAll());
        mm.addAttribute("listRegional", regionalDao.findAll());

        return VIEW_FORM;
    }

    @PostMapping("/form")
    public String prossesForm(ModelMap mm, Group data) {

        groupDao.save(data);
        return "redirect:/master/group/list";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redir) {
        if (StringUtils.hasText(id)) {
            Optional<Group> op = groupDao.findById(id);
            if (op.isPresent()) {
                Group group = op.get();
                if (group != null) {
                    groupDao.deleteById(id);
                }
            }
        }
        return "redirect:/" + VIEW_LIST;
    }
}
