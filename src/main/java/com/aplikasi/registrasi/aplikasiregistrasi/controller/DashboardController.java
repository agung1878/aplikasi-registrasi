package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.GroupDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.MemberGroupDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {

    @Autowired
    private UserDao userDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private MemberGroupDao memberGroupDao;

    @Autowired
    private UserMemberDao userMemberDao;

    @GetMapping("/")
    public String dashboard(Model model, Principal principal, @PageableDefault Pageable pageable) {

        User user = userDao.findByUsername(principal.getName());

        if (user.getRole().getName().equalsIgnoreCase("administrator")) {
            model.addAttribute("group", groupDao.findAll());
            return "dashboard";
        } else {

            UserMember userMember = userMemberDao.findByUserUsername(principal.getName());
            model.addAttribute("data", userMember.getMemberGroupSet());
            return "dashboard_m";
        }
    }

}
