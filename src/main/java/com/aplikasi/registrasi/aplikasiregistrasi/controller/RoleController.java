package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.RoleDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.Role;
import java.util.Optional;
import org.flywaydb.core.internal.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author agung
 */
@Controller
@RequestMapping("system/role")
public class RoleController {
    
    @Autowired
    private RoleDao roleDao;
    
    public static final String VIEW__LIST = "system/role/list";
    public static final String VIEW_FORM = "system/role/form";
    
    public static final Logger logger = LoggerFactory.getLogger(RoleController.class);
    
    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault Pageable pageable){
        mm.addAttribute("datas", roleDao.findAll(pageable));
        
        return VIEW__LIST ;
    }
    
    @GetMapping("/form")
    public String showForm(ModelMap mm, @RequestParam(required = false) Optional<String> id) throws Exception {
        if (id.isPresent()) {
            try {
                roleDao.findById(id.get()).ifPresent(Role -> mm.addAttribute("role", Role));
            } catch (Exception e) {
                logger.error("Data tidak ditemukan");
                throw new Exception("Data tidak ditemukan");
            }
        } else {
            mm.addAttribute("role", new Role());
        }
        return VIEW_FORM;
    }
    
    @PostMapping("/form")
    public String saveForm(Role role) {
        roleDao.save(role);
        return "redirect:/" + VIEW__LIST;
    }
    
    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redir) {
        if (StringUtils.hasText(id)) {
            Optional<Role> op = roleDao.findById(id);
            if (op.isPresent()) {
                Role role = op.get();
                if (role != null) {
                    roleDao.deleteById(id);
                }
            }
        }
        return "redirect:/" + VIEW__LIST;
    }
}
