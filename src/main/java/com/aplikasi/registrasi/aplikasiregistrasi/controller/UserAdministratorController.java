package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.RoleDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserAdminDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserAdmin;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserPassword;
import java.util.Optional;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("system/user_administrator")
@Transactional
public class UserAdministratorController {

    @Autowired
    private UserAdminDao userAdminDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEndcoder;

    private static final String VIEW_LIST = "system/user_administrator/list";
    private static final String VIEW_FORM = "system/user_administrator/form";
    private final Logger logger = LoggerFactory.getLogger(UserAdministratorController.class);

    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault Pageable pageable) {
        mm.addAttribute("datas", userAdminDao.findAll(pageable));

        return VIEW_LIST;
    }

    @GetMapping("/form")
    public String viewForm(ModelMap mm, @RequestParam(required = false) String id) throws Exception {

        UserAdmin userAdmin = new UserAdmin();
        if (id != null && !id.isEmpty()) {
            Optional<UserAdmin> opUserAd = userAdminDao.findById(id);
            if (opUserAd.isPresent()) {
                userAdmin = opUserAd.get();
            }
        } else {
            userAdmin.setUser(new User());
        }

        mm.addAttribute("userAdmin", userAdmin);
        return VIEW_FORM;
    }

    @PostMapping("/form")
    public String saveForm(UserAdmin userAdmin, RedirectAttributes redir, @RequestParam(required = false) String password) {

        if (userAdmin.getId() == null || userAdmin.getId().isEmpty()) {
            User user = new User();
            user.setUsername(userAdmin.getUser().getUsername());
            user.setActive(true);
            user.setRole(roleDao.findById("admin").get());
            user.setUserPassword(new UserPassword(user, passwordEndcoder.encode(password)));
            User saveUser = userDao.save(user);
            userAdmin.setUser(saveUser);
        } else {
            if (password != null && !password.isEmpty()) {
                User user = userDao.findById(userAdmin.getUser().getId()).get();
                user.getUserPassword().setPassword(passwordEndcoder.encode(password));
                userDao.save(user);
            }
        }

        userAdminDao.save(userAdmin);
        return "redirect:/" + VIEW_LIST;
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redir) {
        if (StringUtils.hasText(id)) {
            Optional<UserAdmin> op = userAdminDao.findById(id);
            if (op.isPresent()) {
                UserAdmin userAdmin = op.get();
                if (userAdmin != null) {
                    userAdminDao.deleteById(id);
                    redir.addFlashAttribute("successMessage", "Pegawai : [" + userAdmin.getName() + "] berhasil dihapus");
                }
            }
        }
        return "redirect:/" + VIEW_LIST;
    }
}
