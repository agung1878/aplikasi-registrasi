package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.PermissionDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.Permission;
import java.util.Optional;
import org.flywaydb.core.internal.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author agung
 */
@Controller
@RequestMapping("/system/permission")
public class PermissionController {
    
    @Autowired
    private PermissionDao permissionDao;
    
    public static final String VIEW_LIST = "system/permission/list";
    public static final String VIEW_FORM = "system/permission/form";
    
    public static final Logger logger = LoggerFactory.getLogger(PermissionController.class);
    
    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault Pageable pageable){
        mm.addAttribute("datas", permissionDao.findAll(pageable));
        return VIEW_LIST;
    }
    
    @GetMapping("/form")
    public String showForm(ModelMap mm, @RequestParam(required = false) Optional<String> id) throws Exception {
        if (id.isPresent()) {
            try {
                permissionDao.findById(id.get()).ifPresent(Permission -> mm.addAttribute("permission", Permission));
            } catch(Exception e) {
                logger.error("Data tidak ditemukan");
                throw new Exception("Data tidak ditemukan");
            }
        } else {
            mm.addAttribute("permission", new Permission());
        }
        return VIEW_FORM;
    }
    
    @PostMapping("/form")
    public String saveForm(Permission permission) {
        permissionDao.save(permission);
        return "redirect:/" + VIEW_LIST;
    }
    
    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redir) {
        if (StringUtils.hasText(id)) {
            Optional<Permission> op = permissionDao.findById(id);
            if (op.isPresent()) {
                Permission permission = op.get();
                if (permission != null) {
                    permissionDao.deleteById(id);
                }
            }
        }
        return "redirect:/" + VIEW_LIST;
    }
}
