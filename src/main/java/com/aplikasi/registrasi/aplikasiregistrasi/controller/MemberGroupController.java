package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.constant.MemberGroupStatus;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.GroupDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.MemberGroupDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.Group;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.MemberGroup;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/user")
@Transactional
public class MemberGroupController {

    @Autowired
    GroupDao groupDao;
    @Autowired
    UserMemberDao userMemberDao;
    @Autowired
    MemberGroupDao memberGroupDao;

    @GetMapping("/group")
    public String groupList(Principal principal, ModelMap mm, @PageableDefault Pageable page, @RequestParam(required = false) String id) {
        UserMember userMember = userMemberDao.findByUserUsername(principal.getName());

        List<MemberGroup> mG = new ArrayList<>();
        mG.addAll(userMember.getMemberGroupSet());

        List<String> listOfString = new ArrayList<>();

        List<Group> listGroupWaiting = new ArrayList<>();
        List<Group> listGroupApproved = new ArrayList<>();
        List<Group> listGroupKicked = new ArrayList<>();
        List<Group> listGroupNotApproved = new ArrayList<>();

        for (MemberGroup mGroup : mG) {
            if (MemberGroupStatus.WAITING.equals(mGroup.getMemberGroupStatus())) {
                listGroupWaiting.add(mGroup.getGroup());
            }
            if (MemberGroupStatus.APPROVED.equals(mGroup.getMemberGroupStatus())) {
                listGroupApproved.add(mGroup.getGroup());
            }
            if (MemberGroupStatus.KICKED.equals(mGroup.getMemberGroupStatus())) {
                listGroupKicked.add(mGroup.getGroup());
            }
            if (MemberGroupStatus.NOT_APPROVED.equals(mGroup.getMemberGroupStatus())) {
                listGroupNotApproved.add(mGroup.getGroup());
            }

            listOfString.add(mGroup.getGroup().getId());
        }

        mm.addAttribute("listWaiting", listGroupWaiting);
        mm.addAttribute("listApproved", listGroupApproved);
        mm.addAttribute("listKicked", listGroupKicked);
        mm.addAttribute("listNotApproved", listGroupNotApproved);

        if (listOfString.size() > 0) {
            mm.addAttribute("listAvailable", groupDao.findByIdNotIn(listOfString));
        } else {
            mm.addAttribute("listAvailable", groupDao.findAll(page));
        }

        return "master/group/listGroupUser";
    }

    @GetMapping("/group/join")
    public String joinGroup(Principal principal, @RequestParam String id) {
        UserMember userMember = userMemberDao.findByUserUsername(principal.getName());
        MemberGroup mG = memberGroupDao.findByGroupIdAndUserMemberId(id, userMember.getId());

        Optional<Group> group = groupDao.findById(id);
        if (group.isPresent()) {
            if (mG != null) {
                mG.setMemberGroupStatus(MemberGroupStatus.WAITING);
                memberGroupDao.save(mG);
            } else {
                MemberGroup memberGroup = new MemberGroup();
                memberGroup.setUserMember(userMember);
                memberGroup.setGroup(group.get());
                memberGroup.setJoinDate(new Date());
                memberGroup.setMemberGroupStatus(MemberGroupStatus.WAITING);
                memberGroupDao.save(memberGroup);
            }
        }
        return "redirect:/user/group";
    }
}
