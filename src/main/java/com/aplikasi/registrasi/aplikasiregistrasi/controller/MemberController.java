/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import java.security.Principal;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author agung
 */
@Controller
@RequestMapping("/member")
public class MemberController {
    
    @Autowired UserDao userDao;
    @Autowired UserMemberDao userMemberDao;
    
    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault(size = 10) Pageable page){
        Page<UserMember> result = userMemberDao.findAll(page);
        mm.addAttribute("data", result);
        
        return "member/list";
    }
    
    @GetMapping("/detail")
    public String profile(Model mm, @RequestParam String id) {
        Optional<UserMember> userMember = userMemberDao.findById(id);
        if(userMember.isPresent())
            mm.addAttribute("profile", userMember.get());
        return "member/detail";
    }
    
}
