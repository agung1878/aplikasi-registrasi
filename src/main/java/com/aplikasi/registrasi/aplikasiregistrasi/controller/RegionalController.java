package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.RegionalDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.Regional;
import java.util.Optional;
import org.springframework.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/master/regional")
public class RegionalController {

    @Autowired
    private RegionalDao regionalDao;

    public static final String VIEW_LIST = "master/regional/list";
    public static final String VIEW_FORM = "master/regional/form";

    private static final Logger logger = LoggerFactory.getLogger(RegionalController.class);

    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault Pageable pageable) {
        mm.addAttribute("datas", regionalDao.findAll(pageable));
        return VIEW_LIST;
    }

    @GetMapping("/form")
    public String showForm(ModelMap mm, @RequestParam(required = false) Optional<String> id) throws Exception {
        if (id.isPresent()) {
            try {
                regionalDao.findById(id.get()).ifPresent(Regional -> mm.addAttribute("regional", Regional));
            } catch (Exception e) {
                logger.error("tidak ada data yang ditemukan");
                throw new Exception("tidak ada data yang ditemukan");
            }
        } else {
            mm.addAttribute("regional", new Regional());
        }
        return VIEW_FORM;

    }

    @PostMapping("/form")
    public String saveForm(Regional regional) {
        regionalDao.save(regional);
        return "redirect:/" + VIEW_LIST;
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redir) {
        if (StringUtils.hasText(id)) {
            Optional<Regional> op = regionalDao.findById(id);
            if (op.isPresent()) {
                Regional regional = op.get();
                if (regional != null) {
                    regionalDao.deleteById(id);
                    redir.addFlashAttribute("successMessege", "regional : [" + regional.getName() + "] berhasil dihapus");
                }
            }
        }
        return "redirect:/" + VIEW_LIST;
    }

}
