package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import com.aplikasi.registrasi.aplikasiregistrasi.services.ImageService;
import java.io.File;
import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author agung
 */
@Controller
@RequestMapping("/profile_member")
public class ProfileMemberController {

    @Autowired
    private UserMemberDao userMemberDao;
    
    @Autowired 
    private ImageService imgService;

    private final List<String> FILE_EXTENSION = Arrays.asList("png", "jpg", "jpeg");
    
    private Logger logger = LoggerFactory.getLogger(ProfileMemberController.class);
    
    @Autowired
    private UserDao userDao;

    @GetMapping("/profile")
    public String profile(Principal principal, ModelMap modelMap) {
        User user = userDao.findByUsername(principal.getName());
        UserMember um = userMemberDao.findByUserId(user.getId());
        modelMap.addAttribute("profile", um);
        
        
        return "profile_member/profile";
    }

    @GetMapping("/form")
    public String showProfile(@RequestParam(required = false) String id, ModelMap mm, Principal principal) {
        User user = userDao.findByUsername(principal.getName());
        UserMember um = userMemberDao.findByUserId(user.getId());
        
        mm.addAttribute("profile", um);
        return "profile_member/form";
    }

    @PostMapping("/form")
    public String updateProfile(@ModelAttribute @Valid UserMember um, BindingResult errors, Model model, MultipartFile filePicture) {
        logger.info("Id user = {}", um.getUser().getId());
        Map<String, String> resp = new HashMap<>();
        if (errors.hasErrors()) {
            resp.put("status", "error");
            String errorMsg = "";
            for (Object o : errors.getAllErrors()) {
                errorMsg += " " + o.toString();
            }

            resp.put("message", errorMsg);
            model.addAttribute("profile", um);
        }else {
            if (!filePicture.getOriginalFilename().isEmpty()) {
                String extention = tokenizer(filePicture.getOriginalFilename(), ".");
                if (FILE_EXTENSION.contains(extention.toLowerCase())) {
                    File file = imgService.moveFile(filePicture, ImageService.PROFILE);
                    um.setPictureUrl(file.getName());
                }
            } else {
                if (um.getPictureUrl().isEmpty()) {
                    um.setPictureUrl(null);
                }
            }

            resp.put("status", "success");
            resp.put("message", "Data has been saved successfully");

            userMemberDao.save(um);
        }
        
        
        return "redirect:/profile_member/profile";
    } 
    private String tokenizer(String originalFilename, String token) {
        StringTokenizer tokenizer = new StringTokenizer(originalFilename, token);
        String result = "";
        while (tokenizer.hasMoreTokens()) {
            result = tokenizer.nextToken();
        }
        return result;
    }
}
