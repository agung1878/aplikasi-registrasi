package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author agung
 */
@Controller
public class SignInController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignInController.class);
    @GetMapping("signin")
    public String signIn(ModelMap mm, HttpServletRequest request, HttpSession session, Principal principal){
        if (principal != null){
            return "redirect:/";
        }
        String messageErrorSecurity = "Masukan Username dan Password anda untuk masuk Aplikasi.";
        if (session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION") != null) {
            Exception ex = (Exception) session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
            if (ex.getMessage().contains("disabled")) {
                messageErrorSecurity = "User tidak aktif.";
            }else {
                messageErrorSecurity = "Username / password yang anda masukkan salah.";
            }
        }
        mm.addAttribute("messageErrorSecurity", messageErrorSecurity);
        return "signin";
    }
}
