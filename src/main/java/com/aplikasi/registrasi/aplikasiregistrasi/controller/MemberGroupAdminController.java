package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.constant.MemberGroupStatus;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.GroupDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.MemberGroupDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.MemberGroup;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import com.aplikasi.registrasi.aplikasiregistrasi.services.NotificationService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin/group")
public class MemberGroupAdminController {

    @Autowired
    GroupDao groupDao;

    @Autowired
    UserMemberDao userMemberDao;

    @Autowired
    MemberGroupDao memberGroupDao;

    @Autowired
    private NotificationService notificationService;

    public static final String LIST_REQUEST = "member/group/listRequest";
    public static final String LIST_ACCEPT = "member/group/listJoined";

    private final Logger logger = LoggerFactory.getLogger(MemberGroupAdminController.class);

    @GetMapping("/accept")
    public String acceptGroup(@RequestParam(required = false) String id) {

        Optional<MemberGroup> memberGroup = memberGroupDao.findById(id);
        if (memberGroup.isPresent()) {
            memberGroup.get().setMemberGroupStatus(MemberGroupStatus.APPROVED);
            memberGroupDao.save(memberGroup.get());

            //Sending Email
            try {
                notificationService.sendNotification(memberGroup.get().getUserMember());
                logger.info("Sending email success : ");
            } catch (MailException e) {
                logger.info("Error Sending Email : " + e.getMessage());
            }
        }

        return "redirect:/";
    }

    @GetMapping("/reject")
    public String reject(@RequestParam(required = false) String id) {
        Optional<MemberGroup> memberGroup = memberGroupDao.findById(id);
        if (memberGroup.isPresent()) {
            memberGroup.get().setMemberGroupStatus(MemberGroupStatus.NOT_APPROVED);
            memberGroupDao.save(memberGroup.get());
        }
        return "redirect:/";
    }

    @GetMapping("/kick")
    public String kick(@RequestParam(required = false) String id) {
        Optional<MemberGroup> memberGroup = memberGroupDao.findById(id);
        if (memberGroup.isPresent()) {
            memberGroup.get().setMemberGroupStatus(MemberGroupStatus.KICKED);
            memberGroupDao.save(memberGroup.get());
        }
        return "redirect:/";
    }

    @GetMapping("/listRequest")
    public String listRequest(ModelMap mm, @RequestParam String id, @PageableDefault Pageable page) {
        mm.addAttribute("datas", memberGroupDao.findByGroupIdAndMemberGroupStatus(id, MemberGroupStatus.WAITING, page));
        return LIST_REQUEST;
    }

    @GetMapping("/listJoined")
    public String listJoined(ModelMap mm, @RequestParam String id, @PageableDefault Pageable page) {
        mm.addAttribute("datas", memberGroupDao.findByGroupIdAndMemberGroupStatus(id, MemberGroupStatus.APPROVED, page));
        return LIST_ACCEPT;
    }

    public String kickMemberGroup() {
        return "redirect:/";
    }

}
