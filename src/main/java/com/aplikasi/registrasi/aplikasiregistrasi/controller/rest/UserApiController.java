/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aplikasi.registrasi.aplikasiregistrasi.controller.rest;

import com.aplikasi.registrasi.aplikasiregistrasi.constant.RestResponseStatus;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import com.aplikasi.registrasi.aplikasiregistrasi.dto.RestBaseResponse;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author agung
 */
@RestController
@RequestMapping("/api/user")
public class UserApiController {
   
    @Autowired private UserDao userDao;
    @Autowired private UserMemberDao userMemberDao;
    
    List<String> path = Arrays.asList("username", "email","phone");
    
    
    @GetMapping("/{type}")
    public RestBaseResponse findByEmail(@PathVariable String type, @RequestParam(required = false) String m){
        RestBaseResponse response = new RestBaseResponse();
        if(type != null && path.contains(type)) {
            if (m != null && !m.isEmpty()) {
                UserMember userMember = null;
                switch(type){
                    case "username":{
                        userMember = userMemberDao.findByUserUsername(m);
                        break;
                    }    
                    case "phone":{
                        userMember = userMemberDao.findByPhone(m);
                        break;
                    }
                    case "email":{
                        userMember = userMemberDao.findByEmail(m);
                    }
                }
                if (userMember != null) {
                    response.setStatus(RestResponseStatus.SUCCESS);
                    response.setData(userMember);
                } else {
                    response.setStatus(RestResponseStatus.ERROR);
                    response.setMessage("No user found");
                }
            } else {
                response.setStatus(RestResponseStatus.ERROR);
                response.setMessage("Parameter is null or empty");
            }
        }else{
            response.setStatus(RestResponseStatus.ERROR);
            response.setMessage("Path is null or not supported");
        }
    return response;
    }
}
