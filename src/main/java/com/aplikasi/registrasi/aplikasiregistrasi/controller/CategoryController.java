package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.CategoryDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.Category;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/master/category")
public class CategoryController {

    @Autowired
    private CategoryDao categoryDao;

    public static final String VIEW_LIST = "master/category/list";
    public static final String VIEW_FORM = "master/category/form";

    private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @GetMapping("/list")
    public String showList(ModelMap mm, @PageableDefault Pageable pageable) {
        mm.addAttribute("datas", categoryDao.findAll(pageable));
        return VIEW_LIST;
    }

    @GetMapping("/form")
    public String viewForm(ModelMap mm, @RequestParam(required = false) Optional<String> id) throws Exception {
        if (id.isPresent()) {
            try {
                categoryDao.findById(id.get()).ifPresent(Category -> mm.addAttribute("category", Category));
            } catch (Exception e) {
                logger.error("Data tidak ditemukan");
                throw new Exception("Data tidak ditemukan");
            }
        } else {
            mm.addAttribute("category", new Category());
        }
        return VIEW_FORM;
    }

    @PostMapping("/form")
    public String saveForm(Category category) {
        categoryDao.save(category);
        return "redirect:/" + VIEW_LIST;
    }

    @GetMapping("/delete")
    public String delete(@RequestParam String id, RedirectAttributes redir) {
        if (StringUtils.hasText(id)) {
            Optional<Category> op = categoryDao.findById(id);
            if (op.isPresent()) {
                Category category = op.get();
                if (category != null) {
                    categoryDao.deleteById(id);
                    redir.addFlashAttribute("successMessage", "Pegawai : [" + category.getName() + "] berhasil dihapus");
                }
            }
        }
        return "redirect:/" + VIEW_LIST;
    }
}
