/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aplikasi.registrasi.aplikasiregistrasi.controller;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.RoleDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserPasswordDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.Role;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserPassword;
import com.aplikasi.registrasi.aplikasiregistrasi.dto.SignUp;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.boot.autoconfigure.AutoConfigurationPackages.register;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author agung
 */
@Controller
public class SignUpController {

    @Autowired
    private UserDao userDao;
    
    @Autowired
    private UserMemberDao userMemberDao;

    @Autowired
    private UserPasswordDao userPasswordDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/signup")
    public String signupForm(ModelMap mm) {
        mm.addAttribute("signup", new SignUp());
        return "signup";
    }

    @PostMapping("/signup")
    public String signUpProsses(@ModelAttribute("signup") SignUp signup, ModelMap mm) throws Exception {

        User exist = userDao.findByUsername(signup.getUsername());
        if (exist != null) {
            mm.addAttribute("errorUsername", "Username sudah digunakan.");
            mm.addAttribute("signup", signup);
            return "signup";
        }

        Role roleUser = roleDao.findById("member").get();
        
        User user = new User();
        user.setUsername(signup.getUsername());
        user.setActive(true);
        user.setRole(roleUser);
        user.setUserPassword(new UserPassword(user, passwordEncoder.encode(signup.getPassword())));
        userDao.save(user);
        
        UserMember userMember = new UserMember();
        userMember.setAddress(signup.getAddress());
        userMember.setDateOfBirth(signup.getDateOfBirth());
        userMember.setEmail(signup.getEmail());
        userMember.setPhone(signup.getPhone());
        userMember.setName(signup.getName());
        userMember.setUser(user);
        userMemberDao.save(userMember);

        return "redirect:signin";
    }
}
