package com.aplikasi.registrasi.aplikasiregistrasi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author agung
 */
@Entity
@Table(name = "t_permission")
public class Permission extends BaseEntity{

    @NotNull
    @NotEmpty
    @Column(name = "permission_label", nullable = false)
    private String label;

    @NotNull
    @NotEmpty
    @Column(name = "permission_value", nullable = false, unique = true)
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
