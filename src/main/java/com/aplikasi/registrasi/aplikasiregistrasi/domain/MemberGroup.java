package com.aplikasi.registrasi.aplikasiregistrasi.domain;

import com.aplikasi.registrasi.aplikasiregistrasi.constant.MemberGroupStatus;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "t_member_group", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_group", "id_user"})})
public class MemberGroup extends BaseEntity {

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_user")
    private UserMember userMember;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "id_group")
    private Group group;

    @NotNull
    @Enumerated(EnumType.STRING)
    private MemberGroupStatus memberGroupStatus = MemberGroupStatus.WAITING;

    @Temporal(TemporalType.DATE)
    private Date joinDate;

    public UserMember getUserMember() {
        return userMember;
    }

    public void setUserMember(UserMember userMember) {
        this.userMember = userMember;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public MemberGroupStatus getMemberGroupStatus() {
        return memberGroupStatus;
    }

    public void setMemberGroupStatus(MemberGroupStatus memberGroupStatus) {
        this.memberGroupStatus = memberGroupStatus;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

}
