package com.aplikasi.registrasi.aplikasiregistrasi.configuration;

import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserAdminDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserDao;
import com.aplikasi.registrasi.aplikasiregistrasi.dao.UserMemberDao;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.User;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserAdmin;
import com.aplikasi.registrasi.aplikasiregistrasi.domain.UserMember;
import java.security.Principal;
import javax.servlet.http.HttpSession;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author agung
 */
@ControllerAdvice(basePackages = {"com.aplikasi.registrasi.aplikasiregistrasi.controller"})
public class GlobalControllerAdvice {

    @Autowired
    private UserDao ud;
    @Autowired
    private UserMemberDao umd;
    @Autowired
    private UserAdminDao uad;

    private final Logger logger = LoggerFactory.getLogger(GlobalControllerAdvice.class);

    @ModelAttribute
    public void globalAtrributes(Model model, Principal principal, Authentication authentication, HttpSession session) {

        if (principal != null) {
            String aktifUser = principal.getName();
            User user = ud.findByUsername(aktifUser);
            if (user != null) {
                if (user.getRole().getName().equalsIgnoreCase("administrator")) {
                    model.addAttribute("userLoggedIn", ud.findByUsername(aktifUser));
                    model.addAttribute("member", false);
                } else {
                    model.addAttribute("userLoggedIn", user);
                    model.addAttribute("member", true);
                }
            }
            
            
            UserMember u = umd.findByUserUsername(principal.getName());
            session.setAttribute("um", u);
            
            UserAdmin ua = uad.findByUserUsername(principal.getName());
            session.setAttribute("ua", ua);
        }

    }
}
