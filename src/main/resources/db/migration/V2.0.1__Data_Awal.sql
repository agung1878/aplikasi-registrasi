insert into t_category (id, code, description, name, updated_at) values 
('01', 'K01', 'java', 'Java Spring','2019-08-23'),
('02', 'K02', 'php', 'Codeigniter','2019-08-24'),
('03', 'K03', 'html', 'Bootstrap','2019-08-25'),
('04', 'K04', 'c++', 'Dekstop','2019-08-26'),
('05', 'K05', 'visual basic', 'Dekstop','2019-08-27'),
('06', 'K06', 'css', 'Design','2019-08-28');

insert into t_regional (id, code, name, updated_at) values
('01', 'JT', 'jakarta timur', '2011-01-01'),
('02', 'JS', 'jakarta selatan', '2011-01-02'),
('03', 'JG', 'jogja', '2011-01-03'),
('04', 'ML', 'malang', '2011-01-04'),
('05', 'BW', 'banyuwangi', '2011-01-05');

insert into t_group (id, name, id_category, days, hours, min_age, max_age, id_regional, location, max_member) values
('01', 'Java Group', '01', 'senin', '19.00', '16th', '99th', '01', 'bandung', '01');