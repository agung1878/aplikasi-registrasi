INSERT INTO t_permission (id, permission_label, permission_value) VALUES
('administrator', 'admin', 'ROLE_ADMIN'),
('member', 'member', 'ROLE_USER_MEMBER');

INSERT INTO t_role (id, description, name) VALUES
('admin', 'Administrator', 'Administrator'),
('member', 'Member', 'Member');

INSERT INTO t_role_permission (id_role, id_permission) VALUES
('admin', 'administrator'),
('member', 'member');

INSERT INTO t_user VALUES
('1552c462-03bb-408c-ad1d-0f6e2fd737ba',1,'admin','admin');

INSERT INTO t_user_admin VALUES
('1','admin@yopmail.com','admin','05555', 'default.png', '1999-04-16', 'Laki-laki', '1552c462-03bb-408c-ad1d-0f6e2fd737ba');

INSERT INTO t_user_password VALUES
('1552c462-03bb-408c-ad1d-0f6e2fd737ba','$2a$13$.c90mk3xElIKKYiQu5SRUuRHwcmqhJOG48gRRX8hghM.Zqp6Ra6O2');