create table t_user_password (
       id_user varchar(36) not null,
        password varchar(255) not null,
        primary key (id_user)
    ) engine=InnoDB;
CREATE TABLE `t_permission` (
  `id` varchar(36) NOT NULL,
  `permission_label` varchar(255) NOT NULL,
  `permission_value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jjckk6a111s7o4dnlr42kx5mm` (`permission_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_role` (
  `id` varchar(36) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bkpm7njy2ort1yoiddc7jg8gj` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_role_permission` (
  `id_role` varchar(36) NOT NULL,
  `id_permission` varchar(36) NOT NULL,
  PRIMARY KEY (`id_role`,`id_permission`),
  KEY `FKieb12qxn2c3a4lu5yuaeps2lb` (`id_permission`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_regional` (
  `id` varchar(36) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_category` (
  `id` varchar(36) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_group` (
  `id` varchar(36) NOT NULL,
  `days` varchar(255) NOT NULL,
  `hours` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `max_age` varchar(255) NOT NULL,
  `max_member` int(11) NOT NULL,
  `min_age` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id_category` varchar(36) NOT NULL,
  `id_regional` varchar(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgys2k8o513124rqt282lya0mm` (`id_category`),
  KEY `FKks5oeh8cwb5p7fepkx4g7dnvk` (`id_regional`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_user` (
  `id` varchar(36) NOT NULL,
  `active` bit(1) NOT NULL,
  `username` varchar(255) NOT NULL,
  `id_role` varchar(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jhib4legehrm4yscx9t3lirqi` (`username`),
  KEY `FK3mgw2r5niou501um9otmhoey7` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_user_admin` (
  `id` varchar(36) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `picture_url` varchar(255),
  `date_of_birth` datetime NOT NULL,
  `gender` varchar(255) NOT NULL,
  `id_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_tnfhw6kjf71t7m30p8iw8iy87` (`email`),
  KEY `FKmpumak3liago09iwjxc8egnxa` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_user_member` (
  `id` varchar(36) NOT NULL,
  `picture_url` varchar(50) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `date_of_birth` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKovxs1sh0vba0t3ofq2f06alsw` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `t_member_group` (
  `id` varchar(36) NOT NULL,
  `member_group_status` varchar(36) NOT NULL,
  `join_date` date DEFAULT NULL,
  `id_group` varchar(36) NOT NULL,
  `id_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKml07xrnv8x67akq0yo6o485hr` (`id_group`,`id_user`),
  KEY `FK6q0dryqnbmvoax0utfgy2245c` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table t_member_group 
       add constraint FKlcdb167mj672o6je5y9i15gxj 
       foreign key (id_group) 
       references t_group (id);
    
    alter table t_member_group 
       add constraint FK6q0dryqnbmvoax0utfgy2245c 
       foreign key (id_user) 
       references t_user_member (id);
